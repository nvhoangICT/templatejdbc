package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
	private static final String jdbcURL = "jdbc:mysql://localhost:3306/ten_database";
//	private static final String jdbcURL = "jdbc:oracle:thin:@localhost:1521:ten_database"; // Oracle
//	private static final String jdbcURL = "jdbc:postgresql://localhost:5432/ten_database"; // PostgreSQL
//  private static final String jdbcURL = "jdbc:sqlserver://localhost:1433;databaseName=ten_database"; // MSSQL
	private static final String username = "ten_nguoi_dung";
	private static final String password = "mat_khau";

	public static Connection getConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(jdbcURL, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	// Đóng kết nối
	public static void closeConnection(Connection connection) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// In thông tin kết nối
	public static void printInfo(Connection connection) {
		if (connection != null) {
			try {
				System.out.println("JDBC URL: " + connection.getMetaData().getURL());
				System.out.println("Username: " + connection.getMetaData().getUserName());
				System.out.println("Driver Name: " + connection.getMetaData().getDriverName());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Không có kết nối cơ sở dữ liệu.");
		}
	}
}
