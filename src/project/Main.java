package project;

import java.sql.Connection;

import dao.UserDao;
import model.User;
import util.DatabaseConnection;

public class Main {

	public static void main(String[] args) {
		UserDao userDao = UserDao.getInstance(); // Sử dụng getInstance để lấy thể hiện duy nhất
		Connection connection = null; // Định nghĩa biến kết nối

		try {
			// Mở kết nối đến cơ sở dữ liệu
			connection = DatabaseConnection.getConnection();

			// Thêm người dùng
			User newUser = new User();
			newUser.setUsername("john_doe");
			newUser.setEmail("john@example.com");
			newUser.setPassword("password");
			userDao.add(newUser);

			// Lấy thông tin người dùng
			User retrievedUser = userDao.getById(1);
			System.out.println("Retrieved User: " + retrievedUser.getUsername());

			// Cập nhật thông tin người dùng
			retrievedUser.setEmail("new_email@example.com");
			userDao.update(retrievedUser);

			// Xóa người dùng
			userDao.delete(retrievedUser.getId());
		} finally {
			// Đảm bảo rằng kết nối được đóng sau khi hoàn thành công việc
			if (connection != null) {
				DatabaseConnection.closeConnection(connection);
				DatabaseConnection.printInfo(connection);
			}
		}
	}
}
