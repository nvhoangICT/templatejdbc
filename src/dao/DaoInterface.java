package dao;

import java.util.List;
import java.sql.SQLException;

public interface DaoInterface<T> {
    // Thêm một đối tượng vào cơ sở dữ liệu
    void add(T entity) throws SQLException;

    // Cập nhật thông tin một đối tượng trong cơ sở dữ liệu
    void update(T entity) throws SQLException;

    // Xóa một đối tượng khỏi cơ sở dữ liệu dựa trên ID
    void delete(int id) throws SQLException;

    // Lấy một đối tượng từ cơ sở dữ liệu dựa trên ID
    T getById(int id) throws SQLException;

    // Lấy tất cả các đối tượng từ cơ sở dữ liệu
    List<T> getAll() throws SQLException;
}
