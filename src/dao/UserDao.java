package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;
import util.DatabaseConnection;

public class UserDao implements DaoInterface<User> {
	private Connection connection;
	private static UserDao instance;

	private UserDao() {
		connection = DatabaseConnection.getConnection();
	}

	public static UserDao getInstance() {
		if (instance == null) {
			instance = new UserDao();
		}
		return instance;
	}

	@Override
	public void add(User user) {
		try {
			String query = "INSERT INTO users (username, email, password) VALUES (?, ?, ?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getEmail());
			preparedStatement.setString(3, user.getPassword());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			handleSQLException(e);
		}
	}

	@Override
	public User getById(int id) {
		User user = null;
		try {
			String query = "SELECT * FROM users WHERE id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);

			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				user = new User();
				user.setId(resultSet.getInt("id"));
				user.setUsername(resultSet.getString("username"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
			}
		} catch (SQLException e) {
			handleSQLException(e);
		}
		return user;
	}

	@Override
	public List<User> getAll() {
		List<User> userList = new ArrayList<>();
		try {
			String query = "SELECT * FROM users";
			Statement statement = connection.createStatement();

			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setUsername(resultSet.getString("username"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				userList.add(user);
			}
		} catch (SQLException e) {
			handleSQLException(e);
		}
		return userList;
	}

	@Override
	public void update(User user) {
		try {
			String query = "UPDATE users SET email = ?, password = ? WHERE id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, user.getEmail());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setInt(3, user.getId());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			handleSQLException(e);
		}
	}

	@Override
	public void delete(int userId) {
		try {
			String query = "DELETE FROM users WHERE id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, userId);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			handleSQLException(e);
		}
	}

	private void handleSQLException(SQLException e) {
		e.printStackTrace();
		// Xử lý ngoại lệ SQL tùy chỉnh
	}
}
